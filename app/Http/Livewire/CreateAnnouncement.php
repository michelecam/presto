<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category;
use App\Models\Announcement;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;

class CreateAnnouncement extends Component
{
    use WithFileUploads;

    public $title;
    public $body;
    public $category;
    public $price;
    public $temporary_images;
    public $images = [];
    public $announcement;

    protected $rules = [
        'title' => 'required|min:4',
        'body' => 'required|min:10|max:200',
        'category' => 'required',
        'price' => 'required|numeric',
        'images.*' => 'image|max:1024',
        'temporary_images.*' => 'image|max:1024',
    ];

    protected $messages = [
        'required' => 'Il campo :attribute è richiesto!',
        'min' => 'Il campo :attribute è troppo corto!',
        'max' => 'Il campo :attribute è troppo lungo!',
        'numeric' => 'Il campo :attribute dev\'essere un numero!',
        'temporary_images.required' => 'L\'immagine è richiesta',
        'temporary_images.*.image' => 'I file devono essere immagini',
        'temporary_images.*.max' => 'L\'immagine deve essere inferiore a 1 MB',
        'images.image' => 'I file devono essere immagini',
        'images.max' => 'L\'immagine deve essere inferiore a 1 MB',

    ];

    public function updatedTemporaryImages()
    {
        if($this->validate([
            'temporary_images.*' => 'image|max:1024',
        ])) {
            foreach ($this->temporary_images as $image) {
                $this->images[] = $image;
            }
            }   
    }

    public function removeImage($key)
    {
        if (in_array($key, array_keys($this->images))) {
            unset($this->images[$key]);
        }
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }
 
    public function saveContact()
    {
        $validatedData = $this->validate();
 
        Contact::create($validatedData);
    }


    public function store()
        {
            $this->validate();

            $this->announcement = Category::find($this->category)->announcements()->create($this->validate());
            if(count($this->images)){
                foreach($this->images as $image) {
                    $this->announcement->images()->create(['path'=>$image->store('images', 'public')]);
                }
            }

            session()->flash('message', 'Annuncio inserito correttamente');
            $this->cleanForm();
        }
        
    public function cleanForm()
    {
        $this->title = '';
        $this->body = '';
        $this->category = '';
        $this->images = [];
        $this->temporary_images = [];
        $this->price = '';
    }

    public function render()
    {
        return view('livewire.create-announcement');
    }
}
