<div>
    <div class="form container my-5">
        <div class="row justify-content-center">
            <div class="col-6">

                <h1 class="mb-4">Crea un nuovo annuncio!</h1>

                @if (session()->has('message'))
                    <div class="flex flex-row justify-center my-2 alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif

                <form wire:submit.prevent="store">
                    @csrf

                    {{-- Titolo --}}
                    <div class="mb-3">
                        <label for="title" class="form-label ms-1">Titolo annuncio</label>
                        <input wire:model="title" type="text"
                            class="form-control @error('title') is-invalid @enderror">
                        @error('title')
                            {{ $message }}
                        @enderror
                    </div>

                    {{-- Descrizione --}}
                    <div class="mb-3">
                        <label for="body" class="form-label ms-1">Descrizione</label>
                        <textarea wire:model="body" type="text" class="form-control @error('body') is-invalid @enderror"></textarea>
                        @error('body')
                            {{ $message }}
                        @enderror
                    </div>

                    {{-- Categoria --}}
                    <div class="mb-3">
                        <label for="category" class="form-label ms-1">Categoria</label>
                        <select wire:model.defer="category" id="category"
                            class="form-control @error('category') is-invalid @enderror" aria-placeholder="img">
                            @error('category')
                                {{ $message }}
                            @enderror">
                            <option value="">Scegli la categoria</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    {{-- Prezzo --}}
                    <div class="mb-3">
                        <label for="price" class="form-label ms-1">Prezzo</label>
                        <input wire:model="price" type="number"
                            class="form-control @error('price') is-invalid @enderror">
                        @error('price')
                            {{ $message }}
                        @enderror
                    </div>



                    {{-- Immagine --}}
                    <div class="mb-3">
                        <input wire:model="temporary_images" type="file" name="images" multiple
                            class="form-control @error('temporary_images.*') is-invalid @enderror" />
                        @error('temporary_images.*')
                            <p class="text-danger mt-2">{{ $message }}</p>
                        @enderror
                    </div>

                    @if (!empty($images))
                        <div class="row">
                            <p>Photo preview:</p>
                            <div class="col-12">
                                <div class="row">
                                    @foreach ($images as $key => $image)
                                        <div class="col my-3">
                                            <div class="img-preview mx-auto shadow rounded"
                                                style="background-image: url({{ $image->temporaryUrl() }})"></div>
                                            <button type="button"
                                                class="btn btn-danger shadow d-block text-center mt-2 mx-auto"
                                                wire:click="removeImage({{ $key }})">Cancella</button>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                    <button type="submit" class="btn">Crea annuncio</button>
                </form>

            </div>
        </div>
    </div>
</div>
