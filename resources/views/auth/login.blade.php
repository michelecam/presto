<x-layout>
    <div class="form container my-5">
        <div class="row justify-content-center">
            <div class="col-6">

                <form action="{{ route('login') }}" method="POST">
                    @csrf

                    <div class="mb-3">
                        <label for="emailInput" class="form-label ms-1">Email</label>
                        <input name="email" type="email" class="form-control" id="emailInput">
                    </div>

                    <div class="mb-3">
                        <label for="passwordInput" class="form-label ms-1">Password</label>
                        <input name="password" type="password" class="form-control" id="passwordInput">
                    </div>
                        <button type="submit" class="btn login-btn mt-3">Accedi</button>
                        <p class="mt-4 ms-2">Non hai un account? <a href="/register">Registrati</a></p>
                </form>

            </div>
        </div>
    </div>
</x-layout>
