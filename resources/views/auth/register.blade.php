<x-layout>
    <div class="form container my-5">
        <div class="row justify-content-center">
            <div class="col-6">

                <form action="{{ route('register') }}" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label for="nameInput" class="form-label ms-1">Nome</label>
                        <input name="name" type="text" class="form-control" id="nameInput">
                    </div>

                    <div class="mb-3">
                        <label for="emailInput" class="form-label ms-1">Email</label>
                        <input name="email" type="email" class="form-control" id="emailInput">
                    </div>

                    <div class="mb-3">
                        <label for="passwordInput" class="form-label ms-1">Password</label>
                        <input name="password" type="password" class="form-control" id="passwordInput">
                    </div>

                    <div class="mb-3">
                        <label for="password_confirmation" class="form-label ms-1">Conferma password</label>
                        <input name="password_confirmation" type="password" class="form-control" id="password_confirmation">
                    </div>

                    <button type="submit" class="btn register-btn mt-3">Registrati</button>
                    <p class="mt-4 ms-2">Sei già registrato? <a href="/login">Accedi</a></p>
                </form>

            </div>
        </div>
    </div>
</x-layout>
