    <div class="card col-12 col-md-6 col-lg-3 py-0 px-0 me-1 mb-5">
        <a href="{{ route('announcements.show', compact('announcement')) }}">
            <img src="https://picsum.photos/500" class="card-img">
        </a>
        <div class="card-body pt-0 ps-1">

            <a href="{{ route('announcements.show', compact('announcement')) }}" class="title-link">
                <h5 class="card-title pt-2 mb-0">{{ $announcement->title }}</h5>
            </a>

            <a href="{{ route('categoryShow', ['category' => $announcement->category]) }}"
                class="card-text category-link">{{ $announcement->category->name }}
            </a>

            <p class="card-text">€{{ $announcement->price }}</p>
        </div>
    </div>
