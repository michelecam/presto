<x-layout>
    <x-heading />
    <h2 class="announcement-heading">Ultimi annunci</h2>
    <div class="announcement-container">
        @foreach ($announcements as $announcement)
            <x-card :announcement="$announcement" />
        @endforeach
    </div>
</x-layout>
