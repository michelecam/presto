<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Presto.it</title>
</head>

<body>
    <h1>Qualcuno ha richiesto di lavorare con noi!</h1>
    <p>Nome utente: {{ $user->name }}</p>
    <p>Email: {{ $user->email }}</p>
    <p>Clicca <a href="{{ route('make.revisor', compact('user')) }}">qui</a> per renderlo revisore</p>
</body>

</html>
