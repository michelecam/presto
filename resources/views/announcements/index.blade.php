<x-layout>
        <h1 class="announcement-heading">Tutti gli annunci</h1>
    
        <div class="announcement-container">
            @foreach ($announcements as $announcement)
                <x-card :announcement="$announcement" />
            @endforeach
        </div>

        <div class="container d-flex justify-content-center align-items-center pt-5">
            {{ $announcements->links() }}
        </div>
</x-layout>
