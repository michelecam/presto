<x-layout>
    
    <div class="container mt-5">


        <div class="row">
            <div class="col-4">
                <img src="https://picsum.photos/1000" class="w-100" alt="">
            </div>

            <div class="col-8">
                <a href="{{ route('categoryShow', ['category' => $announcement->category]) }}"
                    class="category-link">{{ $announcement->category->name }}</a>
                <p class="show-title">{{ $announcement->title }}</p>
                <p class="show-price">{{ $announcement->price }} €</p>
                <p class="show-body">Descrizione: {{ $announcement->body }}</p>
            </div>
        </div>

    </div>
</x-layout>
