<x-layout>
    <h1 class="announcement-heading">{{ $category->name }}</h1>
    <div class="announcement-container">

        @forelse ($category->announcements as $announcement)
            <x-card :announcement="$announcement" />
        @empty
            <div class="col-12 text-center">
                <h3>Non sono presenti annunci in questa categoria!</h3>
                <a href="{{ route('announcements.create') }}">
                    <h4>Pubblicane uno </h4>
                </a>
            </div>
        @endforelse

    </div>
</x-layout>
